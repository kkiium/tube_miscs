package com.xf.tube_demo.controller;

import com.xf.tube_demo.common.LoginResult;
import com.xf.tube_demo.pojo.dto.LoginDto;
import com.xf.tube_demo.pojo.request.LoginReq;
import com.xf.tube_demo.service.LoginService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
    //见文档"项目接口设计.md" 第一章:"系统管理"，节:"3.用户管理 e. 用户登录"
    @Autowired
    private LoginService loginService;
    //旧的 接收form-data 的控制器    public LoginResult<LoginDto> UserLogin(String username, String password)
/*
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public LoginResult<LoginDto> UserLogin(String username, String password){
        //return CommonResult.success(loginService.login(username, password));
        return LoginResult.success(loginService.login(username, password));
    }

 */
    //新的 接收requestBody（对应postman boy raw） 的控制器
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public LoginResult<LoginDto> UserLogin(@RequestBody LoginReq request){
        //return CommonResult.success(loginService.login(username, password));
        return LoginResult.success(loginService.login(request.getUsername(), request.getPassword()));
    }
}