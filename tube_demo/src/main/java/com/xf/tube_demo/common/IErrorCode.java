package com.xf.tube_demo.common;

public interface IErrorCode {
    /**
     * 获取错误码
     * @return 错误码
     */
    long getCode();

    /**
     * 获取错误码的信息
     * @return 错误码信息
     */
    String getMessage();
}

