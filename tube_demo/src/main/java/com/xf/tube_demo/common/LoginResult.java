package com.xf.tube_demo.common;

import lombok.Data;
//兼容接口文档
//用户登录 结果对象
@Data
public class LoginResult<T> {
    private boolean success;
    private String message;
    private T data;

    protected LoginResult() {
    }

    protected LoginResult(boolean success, String message, T data) {
        this.success = success;
        this.message = message;
        this.data = data;
    }

    /**
     * 成功返回结果
     *
     * @param data 获取的数据
     */
    public static <T> LoginResult<T> success(T data) {
        return new LoginResult<T>(true, ResultCode.SUCCESS.getMessage(), data);
    }


}

