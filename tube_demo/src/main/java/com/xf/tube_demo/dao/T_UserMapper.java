package com.xf.tube_demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xf.tube_demo.pojo.T_User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface T_UserMapper extends BaseMapper<T_User>{

}
