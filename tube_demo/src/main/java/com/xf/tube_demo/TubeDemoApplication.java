package com.xf.tube_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TubeDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TubeDemoApplication.class, args);
	}

}
