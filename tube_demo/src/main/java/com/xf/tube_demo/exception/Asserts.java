package com.xf.tube_demo.exception;

import com.xf.tube_demo.common.IErrorCode;

public class Asserts {
    public static void fail(String message) {
        throw new com.xf.tube_demo.exception.ApiException(message);
    }

    public static void fail(IErrorCode errorCode) {
        throw new com.xf.tube_demo.exception.ApiException(errorCode);
    }
}

