package com.xf.tube_demo.service;


import com.xf.tube_demo.pojo.T_User;
import com.xf.tube_demo.pojo.dto.LoginDto;

public interface LoginService{

    LoginDto login(String username, String password);

}