package com.xf.tube_demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.xf.tube_demo.dao.T_UserMapper;
import com.xf.tube_demo.exception.Asserts;
import com.xf.tube_demo.pojo.T_User;
import com.xf.tube_demo.pojo.dto.LoginDto;
import com.xf.tube_demo.service.LoginService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService{

    @Autowired
    private T_UserMapper t_UserMapper;

   public LoginDto login(String username, String password){
       QueryWrapper wrapper=Wrappers.<T_User>query();
       wrapper.eq("username", username);
       wrapper.eq("password", password);
       T_User user=t_UserMapper.selectOne(wrapper);
       if(user==null){
           Asserts.fail("此用户为空");
       }
       LoginDto loginDto=new LoginDto();
        loginDto.setUsername(user.getUsername());
        loginDto.setPassword(user.getPassword());
        loginDto.setPhone(user.getPhone());
        loginDto.setSalt(user.getSalt());
        loginDto.setOrgaId(user.getOrgaId());
        //TODO 查询角色和组织信息添加到loginDto里
        return loginDto;
   }
    
}
