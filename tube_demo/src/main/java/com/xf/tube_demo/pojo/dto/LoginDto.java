package com.xf.tube_demo.pojo.dto;

import com.xf.tube_demo.pojo.Organization;
import com.xf.tube_demo.pojo.Role;
import com.xf.tube_demo.pojo.T_User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginDto extends T_User implements Serializable {

    private List<Role> roles;

    private Organization o;

}
