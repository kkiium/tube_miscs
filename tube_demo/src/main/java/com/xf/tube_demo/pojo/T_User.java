package com.xf.tube_demo.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@TableName("t_user")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class T_User implements Serializable {
    @TableId(value = "id",type = IdType.AUTO)
  protected Integer id;
  @TableField("username")
  protected String username;
  @TableField("password")
  protected String password;
  @TableField("salt")
  protected String salt;
  @TableField("phone")
  protected String phone;
  @TableField("orga_id")
  protected Integer orgaId;

}
