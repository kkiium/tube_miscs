package com.xf.tube_demo.pojo.request;

import lombok.Data;

//@RequestBody注解的请求对象
@Data
public class LoginReq {
    protected String username;
    protected String password;
}
