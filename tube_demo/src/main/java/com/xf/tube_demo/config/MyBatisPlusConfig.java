package com.xf.tube_demo.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
@MapperScan("com.xf.tube_demo.dao")
public class MyBatisPlusConfig {
}
